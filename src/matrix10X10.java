
public class matrix10X10 {

	public static final int NUM_LINES = 10;

	public static void main(String[] args) {

		int[][] matrix;
		matrix = new int[NUM_LINES][NUM_LINES];

		for (int i = 0; i < NUM_LINES; i++) {
			for (int j = 0; j < NUM_LINES; j++) {
				matrix[i][j] = 0;
			}
		}

		matrix[0][4] = 1;
		matrix[2][6] = 1;
		matrix[3][1] = 1;
		matrix[8][6] = 1;

		for (int i = 0; i < NUM_LINES; i++) {
			for (int j = 0; j < NUM_LINES; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		int rowsZeros = 0;
		boolean zeros;
		for (int i = 0; i < matrix.length; i++) {
			zeros = true;
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] != 0) {
					zeros = false;
					break;
				}
			}
			if (zeros) {
				rowsZeros++;
			}
		}
		int colsZeros = 0;
		for (int col = 0; col<matrix[0].length; col++) {
			zeros=true;
			for (int row=0; row<matrix.length; row++) {
				if (matrix[row][col] != 0) {
					zeros = false;
					break;
				}
			}
			if (zeros) {
				colsZeros++ ;
			}
		}
		
		System.out.println("Rows all zeros = " + rowsZeros);
		System.out.println("Columns all zeros = " + colsZeros);
	}
}		
