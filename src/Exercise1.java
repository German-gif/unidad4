import java.util.*;

public class Exercise1 {

	public static void main(String[] args) {
		int[]array = new int[10];
		Scanner input = new Scanner(System.in);
		
		for (int i = 0; i < array.length; i++ ) {
			System.out.println("Enter number " + (i+1));
			array[i] = input.nextInt();
		}
		
		for (int i = 9; i >= 0; i--) {
			System.out.println(array[i]+" ");
		}
	}
	


}
