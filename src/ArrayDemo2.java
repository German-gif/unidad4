
public class ArrayDemo2 {

	public static final int NUM_ELEMENTS = 10;
	public static void main(String[] args) {
		// Declaration
		int[] anArray;
		//Instantiation
		anArray = new int[NUM_ELEMENTS];
		//Initialization
		for (int i = 0; i < anArray.length; i++) {
			anArray[i] = (i+1) * 100;
		}
		
		for (int i = 0; i < anArray.length; i++) {
			System.out.println(anArray[i]);
		}

	}

}
