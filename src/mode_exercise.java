import java.util.Scanner;

public class mode_exercise {

	public static final char[] ARRAY = { 't', 'h', 'i', 's', 'i', 's', 'a', 't', 'e', 's', 't', 's', 'e', 'n', 't', 'e',
			'n', 'c', 'e', 'i', 'n', 'e', 'n', 'g', 'l', 'i', 's', 'h' };

	public static void main(String[] args) {
		int countRepeat;
		char charMax = ARRAY[0];
		int frequencyMax = 0;
		char candidate;
		for (int i = 0; i < ARRAY.length; i++) {
			if (ARRAY[i] == '\u00d0') {
				continue;
			}
			candidate = ARRAY[i];
			countRepeat = 1;
			for (int j = i + 1; j < ARRAY.length; j++) {
				if (candidate == ARRAY[j]) {
					countRepeat++;
					ARRAY[j] = '\u00d0';
				}
			}
			if (countRepeat > frequencyMax) {
				charMax = candidate;
				frequencyMax = countRepeat;
			}
		}
		System.out.println("Mode = " + charMax);
		System.out.println("Frequency = " + frequencyMax);
	}

}
