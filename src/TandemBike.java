
public class TandemBike extends Bicycle {
	int numSeats = 2;

	void applyBrakes(int decrement1, int decrement2) {
		applyBrakes(decrement1);
		applyBrakes(decrement2);
	}
	
	void printState() {
		super.printState();
		System.out.println("num seats: " + numSeats);
	}
}
