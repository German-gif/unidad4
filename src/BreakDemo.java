import java.util.Scanner;

public class BreakDemo {

	public static void main(String[] args) {
		
		boolean prime = true;
		int n;
		
		System.out.println("Enter number:");
		Scanner input = new Scanner(System.in);
		n = input.nextInt();
		input.close();
		
		for (int i=2; i < n; i++) {
			if (n % i == 0) {
				prime = false;
				break;
			}
		}
	
		if (prime) {
			System.out.println("Prime");
		} else {
			System.out.println("Not Prime");
		}
	}
}
